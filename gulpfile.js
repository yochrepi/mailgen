var gulp = require('gulp');
	hb = require('gulp-hb');
	handlebars = require('handlebars');
	rename = require('gulp-rename');
	inlineCss = require('gulp-inline-css');
	minify = require('gulp-htmlmin');
 	send = require('gulp-mailgun');
	data = require('gulp-data');
    fs = require('fs');



/*EMAIL VARIABLE*/
	var type = "eShop";
	var subject = "Poletne počitnice tudi na do 6 obrokov";
	var testni_maili = ' ';

	
/*DATE VARIABLE*/
	var date = new Date();
	var options = { weekday: 'long', year: 'numeric', month: 'numeric', day: 'numeric' };
	var d = date.toLocaleDateString('sl', [options]) + " " + date.toLocaleTimeString('sl-SL');



gulp.task('test', function () {
  gulp.src( 'done/index.html') // Modify this to select the HTML file(s)
  .pipe(send({
    key: ' ', //Mailgun API key here
    sender: ' ',
    recipient: testni_maili,
    subject: subject + " - TEST" + ' on ' + d
  }))
    
  .pipe(send({
        key: ' ', //Mailgun API key here
        sender: ' ',
        recipient: [' '],
        subject: subject
  }))
});


gulp.task('mini', function () {
  gulp.src( 'done/index.html')
/* BELOW EDIT SETTINGS FOR CSS INLINER */
//  .pipe(inlineCss({
//	applyStyleTags: true,
//	applyLinkTags: true,
//	preserveMediaQueries: true
//  }))
  .pipe(minify(
    {
    collapseWhitespace: true,
//	   preserveLineBreaks: true,
    maxLineLength: 120,
     preventAttributesEscaping: true
    }
  ))
  .pipe(gulp.dest('done/min'))
  });


gulp.task('compile', function () {
	return gulp
        .src(type + '/layouts/frame.hbs')
         .pipe(hb({
             partials:  type + '/partials/row.hbs',
             data: type + '/data/data.json'
         }))
 		.pipe(rename('index.html'))
        .pipe(gulp.dest('done'), function (err) {
          if (err) throw err;
		})
});



gulp.task('default', ['compile']);
